import notesStore from './store';

const notesApi = {
  addNote: function(note) {
    if (!note) return;
    let notes = notesStore.fetch();
    notes.push({
      id: notesStore.uid++,
      note: note
    });
    notesStore.save(notes);
  },

  deleteNote: function(noteId) {
    if (!noteId) return;
    let notes = notesStore.fetch();
    notesStore.save(notes.filter(note => note.id !== noteId));
  },

  getNote: function(noteId) {
    if (!noteId) return {};
    let notes = notesStore.fetch();
    return notes.filter(note => note.id === noteId)[0];
  },

  updateNote: function(note) {}
};

export default notesApi;
