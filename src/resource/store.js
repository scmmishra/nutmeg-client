const STORAGE_KEY = 'nutmeg-local-store';
const notesStore = {
  fetch: function() {
    var notes = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
    notes.forEach(function(note, index) {
      note.id = index;
    });
    notesStore.uid = notes.length;
    return notes;
  },
  save: function(notes) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(notes));
  }
};

export default notesStore;
