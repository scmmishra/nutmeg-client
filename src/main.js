import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import Icon from '@/components/Interface/Icon';
import UIState from '@/components/Interface/UIState';
import Illustartion from '@/components/Interface/Illustartion';
import notesApi from '@/resource/note';

Vue.config.productionTip = false;
Vue.component('Icon', Icon);
Vue.component('UIState', UIState);
Vue.component('Illustartion', Illustartion);

window.notesApi = notesApi;

new Vue({
  notesApi: notesApi,
  router,
  render: h => h(App)
}).$mount('#app');
